# Example ansible playbook.

This repo contains a simple ansible playbook to install [keydb](https://docs.keydb.dev/docs/) on an ubuntu 20.04 lts server.

## Getting started

1. Make sure [ansible](https://www.ansible.com/) is installed on your machine.

2. Change the `hosts` file to add the ip addresses of the servers you want to administer.

3. Make sure you can authenticate to the server using ssh over public key.

4. Run the following command:


    ```ansible-playbook -i hosts playbook.yml --ask-become-pass -vvv```

### Example screencast
[![asciicast](https://asciinema.org/a/Fx28JnbJC5c5t7RkAgFDEKoQR.svg)](https://asciinema.org/a/Fx28JnbJC5c5t7RkAgFDEKoQR)

### NB

1. Please note the ```ask-become-pass``` command line option is only needed if you need a password for the user to become root.

2. The ```-vvv``` option sets the verbosity level to maximum. You can remove it if you do not need all those messages cluttering your terminal.